from flask import Flask, render_template, abort, request

app = Flask(__name__)

@app.route("/")
def hello():
    return "docker demo!!!"

@app.route("/<path:url>")
def test(url):
    """
    jinja2.exceptions.TemplateNotFound
    """
    
    if "//" in url or "~" in url or ".." in url:
        abort(403)
    return render_template(url), 200


@app.errorhandler(500)
def internal_server_error(e):  # not found
    """
    If the file is not found, Flask will throw a error 500, 
    treat it as a 404 error
    """
    
    return render_template("404.html"), 404

@app.errorhandler(403)  # forbidden
def error_403(e):
    return render_template("403.html"), 403


if __name__ == "__main__":
    app.config['TRAP_HTTP_EXCEPTIONS']=True
    app.register_error_handler(Exception, internal_server_error)
    app.run(debug=True,host='0.0.0.0')
