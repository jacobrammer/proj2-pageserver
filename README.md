CIS 322 Project 2. 

Can be run by using:
docker build -t flask-demo .

docker run -d -p 5000:5000 flask-demo

Behavior:
localhost:5000 will display the default "hello docker"


localhost:5000/trivia.html will display trivia.html, will return 200.


localhost:5000/saaample.html will display the 404 html file, return 404.


localhost:5000/~sample.html will display the 403 html file, return 403. 